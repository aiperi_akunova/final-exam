const mongoose = require('mongoose');
const idValidator = require('mongoose-id-validator');

const PictureSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  building: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Building',
    required: true
  },
  image: {
    type: String,
    required: 'фото обязательно!',
  },

});

PictureSchema.plugin(idValidator);
const Picture = mongoose.model('Picture', PictureSchema);
module.exports = Picture;