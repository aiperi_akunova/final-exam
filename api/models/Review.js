const mongoose = require('mongoose');
const idValidator = require('mongoose-id-validator');

const ReviewSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  building: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Building',
    required: true
  },
  description: {
    type: String,
    required: 'Description is required!',
  },
  food:{
    type: Number,
    required: true,
  },
  service:{
    type: Number,
    required: true,
  },
  interior:{
    type: Number,
    required: true,
  },
  datetime:{
    type: Date,
    default: Date.now,
  },

});

ReviewSchema.plugin(idValidator);
const Review = mongoose.model('Review', ReviewSchema);
module.exports = Review;