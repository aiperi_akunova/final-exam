const mongoose = require('mongoose');
const idValidator = require('mongoose-id-validator');

const BuildingSchema = new mongoose.Schema({
  title:{
    type: String,
    required: 'Title is required',
  },
  description:{
    type: String,
    required: 'Description is required',
  },
  datetime:{
    type: Date,
    default: Date.now,
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  image: {
    type: String,
    required: 'Main photo is required',
  },
  // rating:{
  //   interior:{
  //     type: Number,
  //     required:true,
  //   },
  //   food:{
  //     type: Number,
  //     required:true,
  //   },
  //   service:{
  //     type: Number,
  //     required:true,
  //   }


});

BuildingSchema.plugin(idValidator);
const Building = mongoose.model('Building', BuildingSchema);
module.exports = Building;