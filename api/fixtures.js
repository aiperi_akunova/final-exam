const mongoose = require("mongoose");
const config = require("./config");
const {nanoid} = require("nanoid");
const User = require("./models/User");
const Building = require("./models/Building");
const Picture = require("./models/Picture");
const Review = require("./models/Review");


const run = async () => {
  await mongoose.connect(config.db.url, config.db.options);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [admin, user1, user2,] = await User.create(
    {
      email: "admin@gmail.com",
      password: "12345678",
      role: "admin",
      name: "Admin",
      token: nanoid(),
    },
    {
      email: "user@gmail.com",
      password: "12345678",
      role: "user",
      name: "User",
      token: nanoid(),
    },
    {
      email: "user2@gmail.com",
      password: "12345678",
      role: "user",
      name: "User user",
      token: nanoid(),
    },
  );


  const [place1, place2, place3,] = await Building.create({
      title: 'Place 1',
      description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita incidunt magnam nihil, numquam recusandae unde ut! Animi dolorum laboriosam nostrum officiis perspiciatis possimus provident quae quasi, repudiandae voluptatum. Accusamus, accusantium beatae itaque laboriosam laborum quidem rerum sapiente veniam. Aspernatur atque cupiditate minus voluptatem! Asperiores cum distinctio ea error id illo ipsam libero non, omnis quae quia quis rem repellendus voluptatem.\n',
      image: 'fixtures/1.jpeg',
      user: user1,
      // building: place1,
    }, {
      title: 'Place 2',
      description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita incidunt magnam nihil, numquam recusandae unde ut! Animi dolorum laboriosam nostrum officiis perspiciatis possimus provident quae quasi, repudiandae voluptatum. Accusamus, accusantium beatae itaque laboriosam laborum quidem rerum sapiente veniam. Aspernatur atque cupiditate minus voluptatem! Asperiores cum distinctio ea error id illo ipsam libero non, omnis quae quia quis rem repellendus voluptatem.\n',
      image: 'fixtures/2.png',
      user: user2,
      // building: place2,
    },

    {
      title: 'Place 3',
      description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita incidunt magnam nihil, numquam recusandae unde ut! Animi dolorum laboriosam nostrum officiis perspiciatis possimus provident quae quasi, repudiandae voluptatum. Accusamus, accusantium beatae itaque laboriosam laborum quidem rerum sapiente veniam. Aspernatur atque cupiditate minus voluptatem! Asperiores cum distinctio ea error id illo ipsam libero non, omnis quae quia quis rem repellendus voluptatem.\n',
      image: 'fixtures/3.jpeg',
      user: user1,
      // building: place3,

    }
  );

  const [picture1, picture2, picture3, picture4, picture5, picture6] = await Picture.create({
      user: user1,
      image: 'fixtures/11.jpeg',
      building: place1,
    }, {
      user: user1,
      image: 'fixtures/12.jpeg',
      building: place1,
    }, {
      user: user2,
      image: 'fixtures/13.jpeg',
      building: place2,
    }, {
      user: user2,
      image: 'fixtures/14.jpeg',
      building: place3,
    }, {
      user: user1,
      image: 'fixtures/15.jpeg',
      building: place3,
    }, {
      user: user2,
      image: 'fixtures/16.jpeg',
      building: place2,
    },
  );


  const [review1, review2, review3, review4, review5, review6] = await Review.create({
      user: user1,
      building: place2,
      description: 'The food was excellent and so was the service.  I had the mushroom risotto with scallops which was awesome. My wife had a burger over greens (gluten-free) which was also very good.',
      food: 4,
    service:3,
    interior:2,
    }, {
    user: user1,
    building: place1,
    description: 'The food was excellent and so was the service.  I had the mushroom risotto with scallops which was awesome. My wife had a burger over greens (gluten-free) which was also very good.',
    food: 2,
    service:4,
    interior:5,
    },  {
      user: user2,
      building: place1,
      description: 'We enjoyed the Eggs Benedict served on homemade focaccia bread and hot coffee.  Perfect  service',
      food: 3,
      service:5,
      interior:2,
    },{
      user: user2,
      building: place3,
      description: 'The food was excellent and so was the service.  I had the mushroom risotto with scallops which was awesome. My wife had a burger over greens (gluten-free) which was also very good.',
      food: 2,
      service:4,
      interior:5,
    }
  );


  await mongoose.connection.close();
};

run().catch(console.error);
