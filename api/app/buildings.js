const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');
const Building = require("../models/Building");
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");


const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', async (req, res) => {
  try {
    const buildings = await Building.find();
    res.send(buildings);
  } catch (e) {
    console.log(e.message)
    res.sendStatus(500);
  }
});



// router.get('/ratings/:id', async (req, res) => {
//   try {
//
//     const reviews = await Review.find({building:req.params.id});
//
//
//     res.send(buildings);
//   } catch (e) {
//     res.sendStatus(500);
//   }
// });


router.get('/:id', async (req, res) => {
  try {
    const building = await Building.findById(req.params.id);
    // const reviews = await Review.find({building:req.params.id});
    //
    // if(reviews.length === 0){
    //   res.status(404).send({error: 'Not found'})
    // }
    //
    // const food = reviews.map(obj=>{
    //   if(obj.food){
    //     return obj[food];
    //   }
    // })
    //
    // const service = reviews.map(obj=>{
    //   if(obj.service){
    //     return obj[service];
    //   }
    // })
    //
    // const interior = reviews.map(obj=>{
    //   if(obj.interior){
    //     return obj[interior];
    //   }
    // })
    //
    // console.log('food', food);
    // console.log('service', service);
    // console.log('interior', interior);

    if(!building){
      res.status(404).send({error: 'Place not found'});
    }else{
      res.send(building);
    }
  } catch (e) {
    res.sendStatus(500);
  }
});


router.post('/', auth, permit('user'),upload.single('image'), async (req, res) => {
  try {
    if(req.body.checked === 'false'){
      return res.status(400).send({error: `You need to agree`});
    }

    const buildingData = {
      title: req.body.title,
      description: req.body.description,
      user: req.user._id,
    };

    if(req.file){
      buildingData.image =  'uploads/' + req.file.filename;
    }

    const building = new Building(buildingData);
    await building.save();
    res.send(building);

  } catch (error) {
    res.status(400).send(error);
  }
});



module.exports = router;