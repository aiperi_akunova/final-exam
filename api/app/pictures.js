const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');
const permit = require("../middleware/permit");
const Picture = require("../models/Picture");
const auth = require("../middleware/auth");


const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

router.get('/:id', async (req, res) => {

  try {
    const pictures = await Picture.find({building: req.params.id});
    res.send(pictures);
  } catch (e) {
    res.sendStatus(500);
  }
});


router.post('/',auth,upload.single('image'), async (req, res) => {
  try {
    console.log(req.body);
    const pictureDate = {
      user: req.user._id,
      building: req.body.building,
      image: 'uploads/'+req.file.filename,
    };

    const picture = new Picture(pictureDate);
    await picture.save();
    res.send(picture);

  } catch (error) {
    res.status(400).send(error);
  }
});

module.exports = router;