const express = require('express');
const auth = require("../middleware/auth");
const Review = require("../models/Review");
const permit = require("../middleware/permit");



const router = express.Router();

router.get('/:id', async (req, res) => {

  try {
    const reviews = await Review.find({building: req.params.id}).populate('user','name');
    res.send(reviews);
  } catch (e) {
    res.sendStatus(500);
  }
});


router.post('/',auth, permit('user'), async (req, res) => {
  try {
    const reviewData = {
      user: req.user._id,
      building: req.body.building,
     description: req.body.description,
      food: req.body.food,
      service: req.body.service,
      interior: req.body.interior,
    };

    const review = new Review(reviewData);
    await review.save();
    res.send(review);

  } catch (error) {
    res.status(400).send(error);
  }
});


module.exports = router;