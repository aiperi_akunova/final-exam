import {all} from 'redux-saga/effects';
import usersSagas from "./sagas/usersSagas";
import buildingsSagas from "./sagas/buildingsSagas";
import picturesSagas from "./sagas/picturesSagas";
import reviewsSagas from "./sagas/reviewsSagas";

export function* rootSagas() {
    yield all([
        ...usersSagas,
        ...buildingsSagas,
        ...picturesSagas,
        ...reviewsSagas
    ]);
}