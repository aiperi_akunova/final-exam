import {createSlice} from "@reduxjs/toolkit";

const name = 'pictures'
const initialState = {
    pictures:[],
    picture:{},
    fetchLoading: false,
    createLoading: false,
    createError: null,
    // deleteLoading: false,
    // deleteError:null,
    fetchError: null,
}

const picturesSlice = createSlice({
    name,
    initialState,
    reducers: {
        addPictureRequest(state){
            state.createLoading = true;
        },
        addPictureSuccess(state) {
            state.createLoading = false;
            state.createError = null;
        },
        addPictureFailure(state,action ){
            state.createLoading = false;
            state.createError = action.payload;
        },
        clearReviewError(state){
            state.createError=null;
        },
        fetchPicturesRequest(state){
            state.fetchLoading = true;
        },
        fetchPicturesSuccess(state, action){
            state.fetchLoading = false;
            state.fetchError = null;
            state.pictures = action.payload
        },
        fetchPicturesFailure(state,action){
            state.fetchLoading = false;
            state.fetchError = action.payload;
        },
        // fetchOneBuildingRequest(state){
        //     state.fetchLoading = true;
        // },
        // fetchOneBuildingSuccess(state, action){
        //     state.fetchLoading = false;
        //     state.fetchError = null;
        //     state.building = action.payload
        // },
        // fetchOneBuildingFailure(state,action){
        //     state.fetchLoading = false;
        //     state.fetchError = action.payload;
        // },

    }
});

export default picturesSlice;