import {createSlice} from "@reduxjs/toolkit";

const name = 'reviews'
const initialState = {
    reviews:[],
    fetchLoading: false,
    createLoading: false,
    createError: null,
    // deleteLoading: false,
    // deleteError:null,
    fetchError: null,
}

const reviewsSlice = createSlice({
    name,
    initialState,
    reducers: {
        addReviewRequest(state){
            state.createLoading = true;
        },
        addReviewSuccess(state) {
            state.createLoading = false;
            state.createError = null;
        },
        addReviewFailure(state,action ){
            state.createLoading = false;
            state.createError = action.payload;
        },
        clearReviewError(state){
            state.createError=null;
        },
        fetchReviewsRequest(state){
            state.fetchLoading = true;
        },
        fetchReviewsSuccess(state, action){
            state.fetchLoading = false;
            state.fetchError = null;
            state.reviews = action.payload
        },
        fetchReviewsFailure(state,action){
            state.fetchLoading = false;
            state.fetchError = action.payload;
        },
        // fetchOneBuildingRequest(state){
        //     state.fetchLoading = true;
        // },
        // fetchOneBuildingSuccess(state, action){
        //     state.fetchLoading = false;
        //     state.fetchError = null;
        //     state.building = action.payload
        // },
        // fetchOneBuildingFailure(state,action){
        //     state.fetchLoading = false;
        //     state.fetchError = action.payload;
        // },

    }
});

export default reviewsSlice;