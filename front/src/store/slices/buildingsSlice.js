import {createSlice} from "@reduxjs/toolkit";

const name = 'buildings'
const initialState = {
    buildings:[],
   building:{},
    fetchLoading: false,
    createLoading: false,
    createError: null,
    fetchError: null,
}

const buildingsSlice = createSlice({
    name,
    initialState,
    reducers: {
        addBuildingRequest(state){
            state.createLoading = true;
        },
        addBuildingSuccess(state) {
            state.createLoading = false;
            state.createError = null;
        },
        addBuildingFailure(state,action ){
            state.createLoading = false;
            state.createError = action.payload;
        },
        clearBuildingError(state){
            state.createError=null;
        },
        fetchBuildingRequest(state){
            state.fetchLoading = true;
        },
        fetchBuildingSuccess(state, action){
            state.fetchLoading = false;
            state.fetchError = null;
            state.buildings = action.payload
        },
        fetchBuildingFailure(state,action){
            state.fetchLoading = false;
            state.fetchError = action.payload;
        },
        fetchOneBuildingRequest(state){
            state.fetchLoading = true;
        },
        fetchOneBuildingSuccess(state, action){
            state.fetchLoading = false;
            state.fetchError = null;
            state.building = action.payload
        },
        fetchOneBuildingFailure(state,action){
            state.fetchLoading = false;
            state.fetchError = action.payload;
        },

    }
});

export default buildingsSlice;