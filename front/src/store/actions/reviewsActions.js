import reviewsSlice from "../slices/reviewsSlice";


export const {
addReviewSuccess,
    addReviewFailure,
    addReviewRequest,
    fetchReviewsFailure,
    fetchReviewsRequest,
    fetchReviewsSuccess,
} = reviewsSlice.actions;