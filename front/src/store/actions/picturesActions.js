import picturesSlice from "../slices/pictureSlices";


export const {
    addPictureSuccess,
    addPictureFailure,
    addPictureRequest,
    fetchPicturesSuccess,
    fetchPicturesFailure,
    fetchPicturesRequest,
} = picturesSlice.actions;