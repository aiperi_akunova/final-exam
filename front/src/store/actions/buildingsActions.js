import buildingsSlice from "../slices/buildingsSlice";


export const {
addBuildingRequest,
    addBuildingSuccess,
    addBuildingFailure,
    clearBuildingError,
    fetchBuildingRequest,
    fetchBuildingSuccess,
    fetchBuildingFailure,
    fetchOneBuildingSuccess,
    fetchOneBuildingFailure,
    fetchOneBuildingRequest,
} = buildingsSlice.actions;