import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";
import History from "../../History";
import {root} from "../../paths";
import {
    addBuildingFailure,
    addBuildingRequest,
    addBuildingSuccess, fetchBuildingFailure, fetchBuildingRequest,
    fetchBuildingSuccess, fetchOneBuildingFailure, fetchOneBuildingRequest, fetchOneBuildingSuccess
} from "../actions/buildingsActions";
import {put, takeEvery} from "redux-saga/effects";



export function* addBuildingSaga({payload}) {
    try {
        yield axiosApi.post( '/buildings', payload);
        yield put(addBuildingSuccess());
        History.push(root);
        toast.success('New place was added');

    } catch (error) {
        if (!error.response) {
            toast.error(error.message);
        }
        yield put(addBuildingFailure(error.response.data));
    }
}

export function* getBuildingSagas() {
    try {
        const response = yield axiosApi.get('/buildings');
        yield put(fetchBuildingSuccess(response.data));
    } catch (error) {
        if (!error.response) {
            toast.error(error.message);
        }
        yield put(fetchBuildingFailure(error.response.data));
    }
}

export function* getOneSagas({payload}) {
    try {
        const response = yield axiosApi.get('/buildings/'+payload);
        yield put(fetchOneBuildingSuccess(response.data));
    } catch (error) {
        if (!error.response) {
            toast.error(error.message);
        }
        yield put(fetchOneBuildingFailure(error.response.data));
    }
}



const buildingSaga = [
    takeEvery(addBuildingRequest, addBuildingSaga),
    takeEvery(fetchBuildingRequest, getBuildingSagas),
    takeEvery(fetchOneBuildingRequest, getOneSagas),

];

export default buildingSaga;