import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";

import {put, takeEvery} from "redux-saga/effects";
import {
    addReviewFailure, addReviewRequest,
    addReviewSuccess,
    fetchReviewsFailure,
    fetchReviewsRequest,
    fetchReviewsSuccess
} from "../actions/reviewsActions";


export function* getReviewsSagas({payload}) {
    try {
        const response = yield axiosApi.get('/reviews/'+payload);
        yield put(fetchReviewsSuccess(response.data));
    } catch (error) {
        if (!error.response) {
            toast.error(error.message);
        }
        yield put(fetchReviewsFailure(error.response.data));
    }
}


export function* addReviewSaga({payload}) {
    try {
        yield axiosApi.post( '/reviews', payload);
        yield put(addReviewSuccess());
        toast.success('New review was added');

    } catch (error) {
        if (!error.response) {
            toast.error(error.message);
        }
        yield put(addReviewFailure(error.response.data));
    }
}


const picturesSaga = [
    takeEvery(addReviewRequest, addReviewSaga),
    takeEvery(fetchReviewsRequest, getReviewsSagas),

];

export default picturesSaga;