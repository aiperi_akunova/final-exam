import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";


import {put, takeEvery} from "redux-saga/effects";
import {
    addPictureFailure,
    addPictureRequest,
    addPictureSuccess,
    fetchPicturesFailure, fetchPicturesRequest,
    fetchPicturesSuccess
} from "../actions/picturesActions";

export function* getPicturesSagas({payload}) {
    try {
        const response = yield axiosApi.get('/pictures/'+payload);
        yield put(fetchPicturesSuccess(response.data));
    } catch (error) {
        if (!error.response) {
            toast.error(error.message);
        }
        yield put(fetchPicturesFailure(error.response.data));
    }
}


export function* addPictureSaga({payload}) {
    try {
        yield axiosApi.post( '/pictures', payload);
        yield put(addPictureSuccess());
        toast.success('New picture was added');

    } catch (error) {
        if (!error.response) {
            toast.error(error.message);
        }
        yield put(addPictureFailure(error.response.data));
    }
}


const picturesSaga = [
    takeEvery(addPictureRequest, addPictureSaga),
    takeEvery(fetchPicturesRequest, getPicturesSagas),

];

export default picturesSaga;