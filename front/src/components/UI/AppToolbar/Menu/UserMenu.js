import React from 'react';
import {Link} from "react-router-dom";
import {useDispatch} from "react-redux";
import {makeStyles} from "@mui/styles";
import {Button} from "@mui/material";
import {logout} from "../../../../store/actions/usersActions";
import {apiURL} from "../../../../config";
import {addPlace} from "../../../../paths";


const useStyles = makeStyles(() =>({
    button: {
       borderLeft: "2px solid white",
        marginRight: "5px",
    },
    avatar: {
        width: "50px",
        height: "auto",
        borderRadius: "50%",
    },

}));

const UserMenu = ({user}) => {
    const classes= useStyles();
    const dispatch = useDispatch();
    return (
        <>
            <Button color="inherit" className={classes.button} component={Link} to={'/users/'+user._id}>
                {user.name}!
            </Button>
            <Button color="inherit" className={classes.button} component={Link} to={addPlace}>
                Add new place
            </Button>
            <Button color="inherit" onClick={() => dispatch(logout())} className={classes.button}>
                Log out
            </Button>
        </>
    );
};

export default UserMenu;