import React from 'react';
import {Link} from "react-router-dom";
import {Button} from "@mui/material";


const Anonymous = () => {
    return (
        <>
            <Button component={Link} to="/register" color="inherit">Sign up</Button>
            <Button component={Link} to="/login" color="inherit">Sign in</Button>
        </>
    );
};

export default Anonymous;