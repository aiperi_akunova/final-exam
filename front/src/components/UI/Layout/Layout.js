import React from 'react';
import AppToolbar from "../AppToolbar/AppToolbar";
import {Container, CssBaseline} from "@mui/material";

const Layout = ({children}) => {
    return (
        <>
            <CssBaseline/>
            <AppToolbar/>
            <main>
                <Container>
                    {children}
                </Container>
            </main>
        </>
    );
};

export default Layout;