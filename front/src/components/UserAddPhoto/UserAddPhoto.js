import React, {useEffect, useState} from 'react';
import FileInput from "../UI/FileInput/FileInput";
import {useDispatch, useSelector} from "react-redux";
import {Grid} from "@mui/material";
import ButtonWithProgress from "../UI/ButtonWithProgress/ButtonWithProgress";
import {addPictureRequest, fetchPicturesRequest} from "../../store/actions/picturesActions";
import {makeStyles} from "@mui/styles";

const useStyles = makeStyles(() => ({
   box:{
       width: '50%',
       paddingTop: "40px",
   }
}));

const UserAddPhoto = ({id}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const error = useSelector(state => state.pictures.createError);
    const[userPic,setUserPic]=useState({
        image:null,
        building:'',
    })

    useEffect(()=>{
        setUserPic(prev=>({
        ...prev,
            building: id
    }))
    },[id])


    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setUserPic(prevState => {
            return {...prevState, [name]: file};
        });
    };

    const getFieldError = fieldName => {
        try {
            return error.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    };

    const submitFormHandler = e => {
        e.preventDefault();

        const formData = new FormData();
        Object.keys(userPic).forEach(key => {
            formData.append(key, userPic[key]);
        });

        dispatch(addPictureRequest(formData));
        dispatch(fetchPicturesRequest(id))
    };

    return (
        <Grid
            container
            direction="column"
            spacing={2}
            component="form"
            autoComplete="off"
            onSubmit={submitFormHandler}
            noValidate
            className={classes.box}
        >
            <FileInput
                required
                label="Main Photo"
                name="image"
                onChange={fileChangeHandler}
                error={Boolean(getFieldError('image'))}

            />
            <Grid item xs={12}>
                <ButtonWithProgress
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    // className={classes.submit}
                    // loading={loading}
                    // disabled={disableButton()}
                >
                    Add new picture
                </ButtonWithProgress>
            </Grid>
        </Grid>


    );
};

export default UserAddPhoto;