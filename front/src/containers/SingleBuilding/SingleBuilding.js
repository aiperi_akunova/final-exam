import React, {useEffect, useRef, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {useParams} from "react-router-dom";
import {apiURL} from "../../config";
import Container from "@mui/material/Container";
import {makeStyles} from "@mui/styles";
import {createTheme} from "@mui/material/styles";
import {fetchOneBuildingRequest} from "../../store/actions/buildingsActions";
import {Card, CardMedia, Grid, Rating, Typography} from "@mui/material";
import UserAddPhoto from "../../components/UserAddPhoto/UserAddPhoto";
import {fetchPicturesRequest} from "../../store/actions/picturesActions";
import FormElement from "../../components/UI/Form/FormElement";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";
import {addReviewRequest, fetchReviewsRequest} from "../../store/actions/reviewsActions";
import dayjs from "dayjs";

const theme = createTheme({
    breakpoints: {
        values: {
            sm: 767,
        },
    },
});

const useStyles = makeStyles(() => ({
    container: {
        paddingTop: '100px',
        [theme.breakpoints.down('sm')]: {
            paddingTop: '100px',
        },
    },

    box: {
        borderBottom: '2px solid grey',
        marginBottom: '40px',
    },

    card: {
        height: '100%',
        textAlign: "center"
    },
    media: {
        height: 0,
        paddingTop: '56.25%'
    },

    mainImage: {
        margin: '20px 0',
        maxWidth: '500px',
        height: 'auto',
    },
    addBox: {
        margin: "50px",
        width: '70%',
        backgroundColor: 'lightblue',
        padding: '20px',
        border: '1px dashed midnightBlue'
    },

    itemBox:{
        width: '30%',
        padding: '20px',
        border: "1px solid midnightBlue",
        backgroundColor: 'lightgray'
    }
}));


const SingleBuilding = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const building = useSelector(state => state.buildings.building);
    const pictures = useSelector(state => state.pictures.pictures);
    const error = useSelector(state => state.reviews.createError);
    const reviews = useSelector(state => state.reviews.reviews);

    const user = useSelector(state => state.users.user);
    const {id} = useParams();

    const [review, setReview] = useState({
        description: '',
        building: '',
        food: 0,
        service: 0,
        interior: 0,
    })


    const messagesEndRef = useRef(null);

    useEffect(() => {
        dispatch(fetchOneBuildingRequest(id));
        dispatch(fetchPicturesRequest(id));
        dispatch(fetchReviewsRequest(id))
        setReview(prevState => ({
            ...prevState,
            building: id,
        }))
    }, [dispatch, id]);


    const getFieldError = fieldName => {
        try {
            return error.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    };

    const submitFormHandler = e => {
        e.preventDefault();
        dispatch(addReviewRequest(review));
        dispatch(fetchReviewsRequest(id));
        dispatch(fetchPicturesRequest(id))
    };

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setReview(prevState => {
            return {...prevState, [name]: value};
        });
    };


    return (
        <Container maxWidth="lg" ref={messagesEndRef} className={classes.container}>
            {building && (
                <>
                    <Grid container className={classes.box} justifyContent={"space-between"}>
                        <Grid item>
                            <Typography variant={"h2"}>{building.title}</Typography>
                            <Typography>{building.description}</Typography>
                        </Grid>
                        <Grid item>
                            <img src={apiURL + '/' + building.image} alt="mainpic" className={classes.mainImage}/>
                        </Grid>
                    </Grid>
                    <Grid item container spacing={2} className={classes.box}>
                        <Grid item>
                            <Typography variant={'h4'}>Gallery of this place</Typography>
                        </Grid>
                        <Grid item container spacing={2}>
                            {pictures && pictures.map(p => (
                                <Grid item xs={8} sm={6} md={4} lg={2} key={p._id}>
                                    <Card className={classes.card}>
                                        <CardMedia
                                            image={apiURL + '/' + p.image}
                                            className={classes.media}
                                        />
                                    </Card>
                                </Grid>
                            ))}
                        </Grid>
                    </Grid>

                        <Grid container spacing={2} className={classes.box} justifyContent={"space-between"} >
                            <Typography variant={'h3'}>All reviews of this place</Typography>
                            {reviews &&  reviews.map(r=>(
                                <Grid item container key={r._id} direction={"column"} className={classes.itemBox}>
                                    <Grid item container spacing={2}>
                                        <Grid item>
                                            <Typography>On {dayjs(r.datetime).format('DD/MM/YYYY') }</Typography>
                                        </Grid>
                                        <Grid item>
                                            <Typography>{r.user?.name} said this:</Typography>
                                        </Grid>
                                    </Grid>
                                    <Grid item>
                                        <Typography>
                                            {r.description}
                                        </Typography>
                                    </Grid>
                                    <Grid item container>
                                        <Typography>Interior</Typography>
                                        <Rating
                                            name="interior"
                                            value={Number(r.interior)}
                                            onChange={inputChangeHandler}
                                        />
                                        <Typography>{Number(r.interior)}</Typography>
                                    </Grid>
                                    <Grid item container>
                                        <Typography>Quality of Food</Typography>
                                        <Rating
                                            name="food"
                                            value={Number(r.food)}
                                            onChange={inputChangeHandler}
                                        />
                                        <Typography>{Number(r.food)}</Typography>
                                    </Grid>
                                    <Grid item container>
                                        <Typography>Service Quality</Typography>
                                        <Rating
                                            name="service"
                                            value={Number(r.service)}
                                            onChange={inputChangeHandler}
                                        />
                                        <Typography>{Number(r.service)}</Typography>
                                    </Grid>


                                    </Grid>
                            ))}
                        </Grid>



                    {user && user.role === 'user' && (
                        <Grid
                            container
                            item
                            direction="column"
                            spacing={2}
                            component="form"
                            autoComplete="off"
                            onSubmit={submitFormHandler}
                            noValidate
                            className={classes.addBox}
                        >
                            <Grid item>
                                <Typography variant={'h4'}>Add a review</Typography>
                            </Grid>

                            <FormElement
                                required
                                label="Comment"
                                name="description"
                                value={review.description}
                                onChange={inputChangeHandler}
                                error={getFieldError('description')}
                            />
                            <Grid container direction={"column"} spacing={2}>
                                <Grid item container xs={12} sm={8} md={9} lg={4.5}>
                                    <Typography>Quality of Food</Typography>
                                    <Rating
                                        name="food"
                                        value={Number(review.food)}
                                        onChange={inputChangeHandler}
                                    />
                                </Grid>
                                <Grid item container xs={12} sm={8} md={9} lg={4.5}>
                                    <Typography>Service Quality</Typography>
                                    <Rating
                                        name="service"
                                        value={Number(review.service)}
                                        onChange={inputChangeHandler}
                                    />
                                </Grid>
                                <Grid item container xs={12} sm={8} md={9} lg={4.5}>
                                    <Typography>Interior</Typography>
                                    <Rating
                                        name="interior"
                                        value={Number(review.interior)}
                                        onChange={inputChangeHandler}
                                    />
                                </Grid>
                            </Grid>
                            <Grid item xs={12}>
                                <ButtonWithProgress
                                    type="submit"
                                    fullWidth
                                    variant="contained"
                                    color="primary"
                                    // className={classes.submit}
                                    // loading={loading}
                                    // disabled={disableButton()}
                                >
                                    Add review
                                </ButtonWithProgress>
                            </Grid>

                        </Grid>
                    )}

                    {user && user.role === 'user' && (
                        <Grid item className={classes.box}>
                            <UserAddPhoto id={building._id}/>
                        </Grid>
                    )}
                </>
            )}
        </Container>
    );
};

export default SingleBuilding;