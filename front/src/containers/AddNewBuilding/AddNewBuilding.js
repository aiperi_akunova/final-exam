import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Checkbox, Container, FormControlLabel, Grid, Typography} from "@mui/material";
import FormElement from "../../components/UI/Form/FormElement";
import FileInput from "../../components/UI/FileInput/FileInput";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";
import {makeStyles} from "@mui/styles";
import {createTheme} from "@mui/material/styles";
import {addBuildingRequest, clearBuildingError} from "../../store/actions/buildingsActions";


const theme = createTheme({
    breakpoints: {
        values: {
            sm: 768,
        },
    },
});

const useStyles = makeStyles(() => ({
    submit: {
        margin: '24px 0 16px',
    },

    breakpoints: {
        values: {
            sm: 768,
        },
    },

    container: {
        width: "90%",
        textAlign: 'center',
        margin: "0 auto",
        paddingTop: '150px',
        marginBottom: '30px',
        [theme.breakpoints.down('sm')]: {
            paddingTop: '100px',
        },
    },
}));


const AddNewPlace = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const loading = useSelector(state => state.buildings.createLoading);
    const error = useSelector(state => state.buildings.createError);

    const [building, setBuilding] = useState({
        description: "",
        image: null,
        title: "",
        checked: false,
    });

    console.log(building);

    const [checkbox, setCheckbox] = useState(false);

    const handleChange = event => {
        setCheckbox(event.target.checked);
    };

    const submitFormHandler = e => {
        e.preventDefault();

        const formData = new FormData();
        Object.keys(building).forEach(key => {
            formData.append(key, building[key]);
        });

        dispatch(addBuildingRequest(formData));
        setBuilding({
            description: "",
            image: null,
            title: "",
            checked: false,
        })
    };

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setBuilding(prevState => {
            return {...prevState, [name]: value};
        });
    };

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setBuilding(prevState => {
            return {...prevState, [name]: file};
        });
    };

    const getFieldError = fieldName => {
        try {
            return error.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    };

    useEffect(() => {
        setBuilding(prevState => ({
            ...prevState,
            checked: Boolean(checkbox)
        }))

    }, [checkbox])

    useEffect(() => {

        return () => {
            dispatch(clearBuildingError());
        };
    }, [dispatch])

    const disableButton = () => {
        if (checkbox) {
            return false;
        } else {
            return true;
        }
    }

    return (
        <Container
            component="section"
            maxWidth="md"
            className={classes.container}>
            <Grid
                container
                direction="column"
                spacing={2}
                component="form"
                autoComplete="off"
                onSubmit={submitFormHandler}
                noValidate
            >
                <h3 style={theme.title}>Add new place</h3>

                <FormElement
                    required
                    label="Title"
                    name="title"
                    value={building.title}
                    onChange={inputChangeHandler}
                    error={getFieldError('title')}
                />

                <FormElement
                    required
                    label="Description"
                    name="description"
                    value={building.description}
                    onChange={inputChangeHandler}
                    error={getFieldError('description')}
                />

                <Grid item xs>
                    <FileInput
                        required
                        label="Main Photo"
                        name="image"
                        onChange={fileChangeHandler}
                        error={Boolean(getFieldError('image'))}
                    />

                </Grid>

                <Grid item container justifyContent={"space-between"}>
                    <Grid item>
                        <Typography>
                            By clicking on the submit entry button and submitting this form you will be indicating your
                            consent to receiving marketing communications by email and our privacy policy.
                        </Typography>
                    </Grid>
                    <Grid item>
                        <FormControlLabel
                            control={<Checkbox sx={{my: '1.5em'}}
                                               checked={checkbox}
                                               onChange={handleChange}
                                               inputProps={{'aria-label': 'controlled'}}/>} label="I agree"/>

                    </Grid>
                </Grid>

                {error&&(
                    <Grid item>
                        <p style={{color: "red"}}>{error?.error}</p>
                    </Grid>
                )}

                <Grid item xs={12}>
                    <ButtonWithProgress
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                        loading={loading}
                        disabled={disableButton()}
                    >
                       Add new place
                    </ButtonWithProgress>
                </Grid>
            </Grid>
        </Container>
    );
};

export default AddNewPlace;