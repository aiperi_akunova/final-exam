import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchBuildingRequest} from "../../store/actions/buildingsActions";
import {CircularProgress, Container, Grid} from "@mui/material";
import PictureShowItem from "../../components/pictureShowItem/pictureShowItem";


const Home = () => {
    const dispatch = useDispatch();
    const buildings = useSelector(state => state.buildings.buildings);
    const loading = useSelector(state => state.buildings.fetchLoading);


    useEffect(()=>{
        dispatch(fetchBuildingRequest())
    },[dispatch])

    return (
        <Container maxWidth="md">
            {loading? (
                <Grid container justifyContent="center" alignItems="center">
                    <Grid item>
                        <CircularProgress/>
                    </Grid>
                </Grid>
            ):(
                <Grid container>
                    {buildings && buildings.map(b=>(
                        <PictureShowItem
                            key={b._id}
                            picture={b.image}
                            title={b.title}
                            buildingId={b._id}
                            date={b.datetime}
                        />
                    ))
                    }
                </Grid>

            )}

        </Container>
    );
};

export default Home;