import Layout from "./components/UI/Layout/Layout";
import {Redirect, Route, Routes} from "react-router-dom";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import {useSelector} from "react-redux";
import Home from "./containers/Home/Home";
import {addPlace, singlePlace} from "./paths";
import AddNewBuilding from "./containers/AddNewBuilding/AddNewBuilding";
import SingleBuilding from "./containers/SingleBuilding/SingleBuilding";


const App = () => {
    // const user = useSelector(state => state.users.user);
    //
    // const ProtectedRoute = ({isAllowed, redirectTo, ...props}) => {
    //     return isAllowed ?
    //         <Route {...props}/> :
    //         <Redirect to={redirectTo}/>
    // };

    return (
        <Layout>
            <Routes>
                <Route path="/" element={<Home/>}/>
                <Route path="/register" element={<Register/>}/>
                <Route path="/login" element={<Login/>}/>
                <Route path={addPlace} element={<AddNewBuilding/>}/>
                <Route path={singlePlace} element={<SingleBuilding/>}/>
            </Routes>
        </Layout>
    );
};

export default App;
